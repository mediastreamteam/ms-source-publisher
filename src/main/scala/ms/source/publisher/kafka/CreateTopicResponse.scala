package ms.source.publisher.kafka

sealed trait CreateTopicResponse

object CreateTopicResponse {
  final case class Created(topic: String) extends CreateTopicResponse
  final case class Exists(topic: String) extends CreateTopicResponse
  final case class Failed(topic: String, error: Throwable)
    extends CreateTopicResponse
}
