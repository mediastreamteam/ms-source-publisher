import com.typesafe.sbt.packager.docker.ExecCmd

organization := "io.bitbucket.mediastreamteam"
version in ThisBuild := "0.0.2-SNAPSHOT"

scalaVersion := "2.12.8"

lazy val global = project
  .in(file("."))
  .configs(IntegrationTest)
  .settings(
    name := "ms-source-publisher",
    Defaults.itSettings,
    commonSettings,
    dockerSettings,
    parallelExecution in Test := false,
    publish / skip := true,
    libraryDependencies ++= Seq(
      dependencies.logback,
      dependencies.scalaTest,
      dependencies.kafkaClients,
      dependencies.akka,
      dependencies.akkaTestKit,
      dependencies.akkaStreamsKafka,
      dependencies.akkaStreams,
      dependencies.akkaStreamTestKit,
      dependencies.akkaStreamsSlick,
      dependencies.postgresDriver,
      dependencies.mockitoCore,
      dependencies.testContainers,
      dependencies.kafkaClient,
      dependencies.sourceDomain
    )
  )
  .enablePlugins(
    JavaAppPackaging,
    DockerPlugin
  )

lazy val versions = new {
  val scalaTest = "3.0.5"
  val logback = "1.2.3"
  val mockito = "3.2.4"
  val kafka = "2.3.0"
  val gson = "2.8.5"
  val akkaStreamsSlick = "1.1.0"
  val akka = "2.5.23"
  val akkaStreamsKafka = "1.0.5"
  val postgresDriver = "42.2.6"
  val testContainers = "0.33.0"
  val kafkaClient = "2.3.1"
  val sourceDomain = "0.0.2-SNAPSHOT"
}

lazy val dependencies = new {

  val kafkaClients = "org.apache.kafka" % "kafka-clients" % versions.kafka
  val gson = "com.google.code.gson" % "gson" % versions.gson
  val logback = "ch.qos.logback" % "logback-classic" % versions.logback
  val scalaTest = "org.scalatest" %% "scalatest" % versions.scalaTest % "it, test"
  val mockitoCore = "org.mockito" % "mockito-core" % versions.mockito % "test"

  val akka = "com.typesafe.akka" %% "akka-actor" % versions.akka
  val akkaTestKit = "com.typesafe.akka" %% "akka-testkit" % versions.akka % "it, test"
  val akkaStreams = "com.typesafe.akka" %% "akka-stream" % versions.akka
  val akkaStreamTestKit = "com.typesafe.akka" %% "akka-stream-testkit" % versions.akka % "test"
  val akkaStreamsSlick = "com.lightbend.akka" %% "akka-stream-alpakka-slick" % versions.akkaStreamsSlick
  var akkaStreamsKafka = "com.typesafe.akka" %% "akka-stream-kafka" % versions.akkaStreamsKafka
  val postgresDriver = "org.postgresql" % "postgresql" % versions.postgresDriver

  val testContainers = "com.dimafeng" %% "testcontainers-scala" % versions.testContainers % "it"
  val kafkaClient = "org.apache.kafka" % "kafka-clients" % versions.kafkaClient % "it, test"

  val sourceDomain = "io.bitbucket.mediastreamteam" %% "ms-source-domain" % versions.sourceDomain
}

// *****************************************************************************
// Aliases
// *****************************************************************************

// Run tests with coverage, optionally add 'it:test' if the project has
// integration tests
addCommandAlias(
  "testCoverage",
  "; coverage ; test; it:test ; coverageReport"
)

// *****************************************************************************
// Settings
// *****************************************************************************

lazy val commonSettings: SettingsDefinition = scoverageSettings ++
  dockerSettings

// -----------------------------------------------------------------------------
// scoverage settings
// -----------------------------------------------------------------------------

lazy val scoverageSettings = Seq(
  coverageMinimum := 80,
  coverageFailOnMinimum := false
)

// -----------------------------------------------------------------------------
// docker settings
// -----------------------------------------------------------------------------


lazy val dockerSettings = Seq(
  dockerRepository := Option("mcelayir"),
  version in Docker := version.value
)
