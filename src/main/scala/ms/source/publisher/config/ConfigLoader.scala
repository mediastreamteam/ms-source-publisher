package ms.source.publisher.config

import com.typesafe.config.Config

trait ConfigLoader {

  implicit def config: Config

  lazy val kafkaAdminConfig: KafkaAdminConfig =
    KafkaAdminConfig.fromConfig(
      config.getConfig("akka.kafka.producer.kafka-clients")
    )
  lazy val producerConfig = config.getConfig("akka.kafka.producer")

  lazy val streamConfig: StreamConfig =
    StreamConfig.fromConfig(config.getConfig("stream"))

}
