package ms.source.publisher.kafka

import ms.source.publisher.config.{KafkaAdminConfig, TopicConfig}
import org.apache.kafka.clients.admin.{AdminClient, NewTopic}
import org.apache.kafka.common.errors.TopicExistsException

import scala.collection.JavaConverters._
import scala.util.{Failure, Success, Try}
import CreateTopicResponse._

class KafkaTopicCreator(adminClient: AdminClient) {

  def createTopic(topicConfig: TopicConfig): CreateTopicResponse = {
    val topicTeplate = new NewTopic(
      topicConfig.name,
      topicConfig.numberOfPartitions,
      topicConfig.replicationFactor.shortValue()
    )

    val newTopic = topicConfig.compactConfig match {
      case Some(compactConfig) =>
        topicTeplate.configs(compactConfig.configs.asJava)
      case None => topicTeplate
    }

    createTopic(newTopic)
  }

  private def createTopic(topic: NewTopic): CreateTopicResponse = {
    val newTopic = List(topic).asJava

    val result = Try {
      adminClient.createTopics(newTopic).all().get()
    }

    kafkaFutureToResponse(topic.name(), result)
  }

  private def kafkaFutureToResponse(
                                     topic: String,
                                     kafkaFuture: Try[Void]
                                   ): CreateTopicResponse =
    kafkaFuture match {
      case Success(_) =>
        Created(topic)
      case Failure(error) =>
        error.getCause match {
          case _: TopicExistsException =>
            Exists(topic)
          case _ =>
            Failed(topic, error)
        }
    }
}

object KafkaTopicCreator {
  def apply(kafkaAdminConf: KafkaAdminConfig): KafkaTopicCreator =
    new KafkaTopicCreator(AdminClient.create(kafkaAdminConf.props))
}
