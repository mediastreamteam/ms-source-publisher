package ms.source.publisher.config

import java.util.Properties

import com.typesafe.config.Config
import org.apache.kafka.clients.admin.AdminClientConfig

case class KafkaAdminConfig(bootstrapServers: String, requestTimeOutMs: Int) {
  lazy val props: Properties = {
    val props = new java.util.Properties()

    props.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers)

    props.put(
      AdminClientConfig.REQUEST_TIMEOUT_MS_CONFIG,
      requestTimeOutMs.toString
    )
    props
  }
}

object KafkaAdminConfig {
  def fromConfig(config: Config): KafkaAdminConfig = KafkaAdminConfig(
    config.getString("bootstrap-servers"),
    config.getInt("request-timeout-ms")
  )
}
