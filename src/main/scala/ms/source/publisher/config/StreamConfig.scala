package ms.source.publisher.config

import java.util.concurrent.TimeUnit

import com.typesafe.config.Config

import scala.concurrent.duration.FiniteDuration

case class StreamConfig(name: String,
                        interval: FiniteDuration,
                        initialDelay: FiniteDuration,
                        maxRetries: Int,
                        minBackoff: FiniteDuration,
                        maxBackoff: FiniteDuration,
                        randomFactor: Double,
                        topic: TopicConfig)

object StreamConfig {
  def fromConfig(config: Config): StreamConfig = StreamConfig(
    config.getString("name"),
    FiniteDuration(
      config.getDuration("interval").toMillis,
      TimeUnit.MILLISECONDS
    ),
    FiniteDuration(
      config.getDuration("initial-delay").toMillis,
      TimeUnit.MILLISECONDS
    ),
    config.getInt(" max-retries"),
    FiniteDuration(
      config.getDuration("min-backoff").toMillis,
      TimeUnit.MILLISECONDS
    ),
    FiniteDuration(
      config.getDuration("max-backoff").toMillis,
      TimeUnit.MILLISECONDS
    ),
    config.getDouble("random-factor"),
    TopicConfig.fromConfig(config.getConfig("topic"))
  )
}
