package ms.source.publisher.actor

import akka.actor.{Actor, ActorLogging, Props, Terminated}

class SupervisorActor extends Actor with ActorLogging {

  override def receive: Receive = {
    case (name: String, props: Props) =>
      val actor = context.actorOf(props, s"$name")
      context.watch(actor)
    case Terminated(actor) =>
      log.warning("Terminated actor: {}", actor.path.name)
      context.system.terminate()
    case any: Any =>
      log.warning("Unexpected message received: {}", any.getClass.getSimpleName)
  }
}

