package ms.source.publisher.stream

import akka.Done
import akka.actor.ActorSystem

import scala.concurrent.Future

trait DataStream {
  def run: Future[Done]
}

object DataStream {
  def stream(topic: String)(implicit system: ActorSystem): DataStream =
    SourceStream.stream(topic)
}
