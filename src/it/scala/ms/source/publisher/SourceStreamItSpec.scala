package ms.source.publisher

import java.io.File
import java.time
import java.util.Properties

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import com.dimafeng.testcontainers.{DockerComposeContainer, ExposedService, ForAllTestContainer}
import com.typesafe.config.{ConfigFactory, ConfigValueFactory}
import ms.source.domain.model.Source
import ms.source.publisher.stream.SourceStream
import org.scalatest.{Matchers, WordSpecLike}
import org.apache.kafka.clients.consumer.KafkaConsumer


class SourceStreamItSpec
  extends TestKit(ActorSystem("SourceStreamSpec"))
    with WordSpecLike
    with Matchers
    with ForAllTestContainer {

  implicit val executionContext = system.dispatcher
  implicit val materializer = ActorMaterializer()


  override val container = DockerComposeContainer(
    new File("integration/docker-compose.yml"),
    exposedServices =
      Seq(ExposedService("postgres_1", 5432), ExposedService("kafka_1", 19092))
  )

  val kafkaBootstrap = "localhost:19092"
  val config = ConfigFactory
    .load()
    .withValue(
      "akka.kafka.kafka-clients.bootstrap-servers",
      ConfigValueFactory.fromAnyRef(kafkaBootstrap)
    )
    .withValue(
      "slick-postgres.db.properties.url",
      ConfigValueFactory
        .fromAnyRef("jdbc:postgresql://127.0.0.1:5432/sources-db")
    )

  "Source stream" should {
    "able to read from db and publish to kafka" in {
      assert(container.getServicePort("postgres_1", 5432) > 0)
      assert(container.getServicePort("kafka_1", 19092) > 0)

      val topic = "test-topic"
      val consumer = SourceConsumer.consumer(kafkaBootstrap)

      val sourceStream = SourceStream.stream(topic).run

      consumer.subscribe(java.util.Arrays.asList(topic))

      val records = consumer.poll(time.Duration.ofSeconds(10))
      records.count() shouldBe 12
      consumer.close()
    }
  }
}

object SourceConsumer {

  val props: String => Properties = (bs) => {
    val properties = new Properties()
    properties.put("bootstrap.servers", bs)
    properties.put("group.id", "test_consumer")
    properties.put("auto.offset.reset", "earliest")
    properties.put(
      "key.deserializer",
      "org.apache.kafka.common.serialization.StringDeserializer"
    )
    properties.put(
      "value.deserializer",
      "ms.source.domain.serialization.SourceDeserializer"
    )
    properties
  }

  def consumer(bootstrapServers: String) =
    new KafkaConsumer[String, Source](props(bootstrapServers))
}
