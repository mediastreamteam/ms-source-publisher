package ms.source.publisher

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import akka.stream.alpakka.slick.javadsl.SlickSession

import scala.util.{Failure, Success}

object Main extends App with Bootstrap {}

