package ms.source.publisher.stream

import akka.actor.ActorSystem
import akka.stream.alpakka.slick.scaladsl.SlickSession
import akka.testkit.TestKit
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{Millis, Span}
import ms.source.domain.model.{Source => DomainSource}

class SourceStreamSpec
  extends TestKit(ActorSystem("SourceStreamSpec"))
    with WordSpecLike
    with Matchers
    with ScalaFutures
    with BeforeAndAfterAll {

  // patience config
  private val patienceTimeoutMs = 5000 // in ms
  private val patienceIntervalMs = 200 // in ms
  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(
      timeout = scaled(Span(patienceTimeoutMs, Millis)),
      interval = scaled(Span(patienceIntervalMs, Millis))
    )

}
