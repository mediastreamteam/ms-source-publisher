package ms.source.publisher

import akka.actor.{ActorSystem, Props}
import akka.stream.ActorMaterializer
import com.typesafe.config.Config
import config.ConfigLoader
import kafka.KafkaTopicCreator
import ms.source.publisher.actor.{StreamActor, SupervisorActor}
import ms.source.publisher.kafka.CreateTopicResponse.Failed
import stream.DataStream

import scala.concurrent.ExecutionContext

trait Bootstrap extends ConfigLoader {

  implicit val actorSystem = ActorSystem("system")
  implicit val materializer = ActorMaterializer()
  implicit val ex: ExecutionContext = actorSystem.dispatcher
  override implicit def config: Config = actorSystem.settings.config

  val log = actorSystem.log

  val topicCreator: KafkaTopicCreator = KafkaTopicCreator(kafkaAdminConfig)

  topicCreator.createTopic(streamConfig.topic) match {

    case Failed(topic, error) =>
      log.error(
        "Unable to create the topic: [{}]. Error:{}. Terminating the application",
        topic,
        error.getMessage
      )
      actorSystem.terminate()
    case _ => log.info("Topic creation successful")
  }

  def streamFactory(): DataStream =
    DataStream.stream(streamConfig.topic.name)

  val props: Props = StreamActor.props(streamConfig, streamFactory)

  val supervisorActor = actorSystem.actorOf(Props(new SupervisorActor))
  supervisorActor ! (streamConfig.name, props)
}
