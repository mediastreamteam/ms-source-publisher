package ms.source.publisher.stream

import akka.actor.ActorSystem
import akka.kafka.ProducerSettings
import akka.kafka.scaladsl.Producer
import akka.stream.ActorMaterializer
import akka.stream.alpakka.slick.scaladsl.{Slick, SlickSession}
import akka.stream.scaladsl.{Sink, Source}
import akka.{Done, NotUsed}
import ms.source.domain.event.SourceUpdateEvent
import ms.source.domain.model.{Source => DomainSource}
import ms.source.domain.serialization.SourceSerializer
import ms.source.domain.serialization.SourceUpdateEventSerde.SourceUpdateEventSerializer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer

import scala.concurrent.Future

object SourceStream {

  object SourceInputStream {
    val session = SlickSession.forConfig("slick-postgres")

    import session.profile.api._

    private type SourceDataModel = (Int, String, String, String, Int, String)

    class SourceTable(tag: Tag) extends Table[SourceDataModel](tag, "source") {
      def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

      def name = column[String]("name")

      def link = column[String]("link")

      def thumbnail = column[String]("thumbnail")

      def refreshFrequencyHours = column[Int]("refresh_frequency_hours")

      def sourceType = column[String]("type")

      override def * =
        (id, name, link, thumbnail, refreshFrequencyHours, sourceType)
    }

    def toSource(input: SourceDataModel): DomainSource =
      DomainSource(input._1, input._2, input._3, input._4, input._5, input._6)

    def source(
                topic: String
              ): Source[ProducerRecord[String, SourceUpdateEvent], NotUsed] =
      Slick
        .source(TableQuery[SourceTable].result)(session)
        .map(toSource)
        .map(source => SourceUpdateEvent(source))
        .map { sourceUpdateEvent =>
          val domainSource = sourceUpdateEvent.source
          new ProducerRecord[String, SourceUpdateEvent](
            topic,
            domainSource.id.toString,
            sourceUpdateEvent
          )
        }
  }

  object SourceOutputStream {

    def sink(
              implicit system: ActorSystem
            ): Sink[ProducerRecord[String, SourceUpdateEvent], Future[Done]] = {

      val producerConfig =
        system.settings.config.getConfig("akka.kafka.producer")

      Producer.plainSink(
        ProducerSettings(
          producerConfig,
          new StringSerializer,
          new SourceUpdateEventSerializer()
        ).withBootstrapServers(
          producerConfig.getString("kafka-clients.bootstrap-servers")
        )
      )
    }
  }

  def stream(topic: String)(implicit system: ActorSystem): DataStream = {
    implicit val materializer = ActorMaterializer()

    new DataStream {
      override def run: Future[Done] =
        SourceInputStream.source(topic).runWith(SourceOutputStream.sink)
    }
  }
}
