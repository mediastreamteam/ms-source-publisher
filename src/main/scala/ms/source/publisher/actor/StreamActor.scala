package ms.source.publisher.actor

import akka.actor.{Actor, ActorLogging, OneForOneStrategy, Props, SupervisorStrategy}
import akka.pattern.{BackoffOpts, BackoffSupervisor}
import ms.source.publisher.config.StreamConfig
import ms.source.publisher.stream.DataStream

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext
import scala.util.control.NonFatal

class StreamActor(initialDelay: FiniteDuration = 1.seconds,
                  interval: FiniteDuration,
                  streamFactory: () => DataStream)
  extends Actor
    with ActorLogging {

  private implicit val ex: ExecutionContext = context.system.dispatcher
  private case object Run

  lazy val stream: DataStream = streamFactory()

  override def preStart(): Unit = {
    super.preStart()
    log.info("Starting stream")
    schedule(initialDelay)
  }

  override def receive: Receive = {
    case Run => runStream()
  }

  def runStream(): Unit =
    stream.run.onComplete(_ => schedule(interval))

  def schedule(delay: FiniteDuration) =
    context.system.scheduler.scheduleOnce(delay) {
      self ! Run
    }
}

object StreamActor {

  def props(streamConfig: StreamConfig,
            streamFactory: () => DataStream): Props =
    BackoffSupervisor.props(
      BackoffOpts
        .onFailure(
          Props(
            new StreamActor(
              streamConfig.initialDelay,
              streamConfig.interval,
              streamFactory
            )
          ),
          childName = streamConfig.name,
          minBackoff = streamConfig.minBackoff,
          maxBackoff = streamConfig.maxBackoff,
          randomFactor = streamConfig.randomFactor
        )
        .withAutoReset(streamConfig.maxBackoff)
        .withSupervisorStrategy(
          OneForOneStrategy(
            maxNrOfRetries = streamConfig.maxRetries,
            withinTimeRange = streamConfig.maxBackoff
          ) {
            case NonFatal(_) => SupervisorStrategy.Restart
          }
        )
    )

}
