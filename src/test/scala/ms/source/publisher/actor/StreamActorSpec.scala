package ms.source.publisher.actor

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import org.scalatest.concurrent.Eventually
import org.scalatest.time.{Millis, Span}

import scala.concurrent.duration._
import org.mockito.Mockito._
import StreamActorSpec._
import akka.Done
import akka.stream.Supervision.Stop
import ms.source.publisher.config.{CompactConfig, StreamConfig, TopicConfig}
import ms.source.publisher.stream.DataStream
import org.mockito.invocation.InvocationOnMock
import org.mockito.stubbing.Answer

import scala.concurrent.Future

class StreamActorSpec
  extends TestKit(ActorSystem("SourceStreamSpec"))
    with WordSpecLike
    with Matchers
    with Eventually
    with BeforeAndAfterAll {

  implicit override val patienceConfig: PatienceConfig =
    PatienceConfig(
      timeout = scaled(Span(patienceTimeoutMs, Millis)),
      interval = scaled(Span(patienceIntervalMs, Millis))
    )

  "Stream actor" should {

    "run stream" in {

      val mockDataStream = mock(classOf[DataStream])
      when(mockDataStream.run).thenReturn(Future.successful(Done))

      val props =
        StreamActor.props(defaultStreamConfig, () => mockDataStream)

      val sut = system.actorOf(props)

      eventually {
        verify(mockDataStream, times(1)).run
      }

      sut ! Stop
    }

    "recover if faced nonfatal exception" in {

      val mockDataStream = mock(classOf[DataStream])

      when(mockDataStream.run).thenAnswer(new Answer[Future[Done]] {
        var counter = 0
        override def answer(invocation: InvocationOnMock): Future[Done] = {
          if (counter == 0) {
            counter += 1
            throw new Exception("something went wrong")
          } else {
            Future.successful(Done)
          }
        }
      })

      val props =
        StreamActor.props(defaultStreamConfig, () => mockDataStream)

      val sut = system.actorOf(props)

      eventually {
        verify(mockDataStream, times(2)).run
      }

      sut ! Stop
    }

    "stop if fails more than max retries" in {

      val mockDataStream = mock(classOf[DataStream])

      when(mockDataStream.run).thenThrow(new NullPointerException)

      val props =
        StreamActor.props(defaultStreamConfig, () => mockDataStream)

      val sut = system.actorOf(props)

      val probe = TestProbe()
      probe.watch(sut)

      eventually {
        probe.expectTerminated(sut)
      }

      sut ! Stop
    }

    "schedule itsef for the next run" in {

      val mockDataStream = mock(classOf[DataStream])

      when(mockDataStream.run).thenReturn(Future.successful(Done))

      val props =
        StreamActor.props(defaultStreamConfig, () => mockDataStream)

      val sut = system.actorOf(props)

      eventually {
        verify(mockDataStream, atLeastOnce()).run
      }

      sut ! Stop
    }
  }

  override def afterAll(): Unit = {
    system.terminate()
  }
}

object StreamActorSpec {

  val patienceTimeoutMs = 5000 // in ms
  val patienceIntervalMs = 200 // in ms

  val defaultTopicName: String = "topic"
  val defaultNumberOfPartitions: Int = 1
  val defaultReplicationFactor: Int = 10
  val defaultCompactConfig: Option[CompactConfig] = None

  val defaultTopicConfig: TopicConfig = new TopicConfig(
    defaultTopicName,
    defaultNumberOfPartitions,
    defaultReplicationFactor,
    defaultCompactConfig
  )

  val defaultStreamName = "test"
  val defaultInitialDelay: FiniteDuration = 100.millis
  val defaultInterval: FiniteDuration = 500.millis
  val defaultMaxRetries: Int = 3
  val defaultMinBackoff: FiniteDuration = 100.millis
  val defaultMaxBackoff: FiniteDuration = 500.millis
  val defaultRandomFactor: Double = 0.2

  val defaultStreamConfig: StreamConfig = new StreamConfig(
    defaultStreamName,
    defaultInterval,
    defaultInitialDelay,
    defaultMaxRetries,
    defaultMinBackoff,
    defaultMaxBackoff,
    defaultRandomFactor,
    defaultTopicConfig
  )
}

