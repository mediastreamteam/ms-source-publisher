package ms.source.publisher.config

import com.typesafe.config.Config

import scala.util.Try

case class TopicConfig(name: String,
                       numberOfPartitions: Int,
                       replicationFactor: Int,
                       compactConfig: Option[CompactConfig])

object TopicConfig {
  def fromConfig(config: Config): TopicConfig = TopicConfig(
    config.getString("name"),
    config.getInt("number-of-partitions"),
    config.getInt("replication-factor"),
    Try(CompactConfig.fromConfig(config.getConfig("compact"))).toOption
  )
}

case class CompactConfig(cleanUpPolicy: String,
                         deleteRetentionMs: Int,
                         segmentMs: Int,
                         minCleanableDirtyRatio: Double) {
  val configs: Map[String, String] = Map(
    "cleanup.policy" -> "compact",
    "delete.retention.ms" -> deleteRetentionMs.toString,
    "segment.ms" -> deleteRetentionMs.toString,
    "max.compaction.lag.ms" -> deleteRetentionMs.toString,
    "message.timestamp.difference.max.ms" -> deleteRetentionMs.toString
  )
}

object CompactConfig {
  def fromConfig(config: Config): CompactConfig = CompactConfig(
    config.getString("cleanup-policy"),
    config.getInt("delete-retention-ms"),
    config.getInt("segment-ms"),
    config.getDouble("min-cleanable-dirty-ratio")
  )
}
