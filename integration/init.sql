
CREATE TABLE source (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100),
    link TEXT,
    thumbnail TEXT,
    refresh_frequency_hours INT DEFAULT 24,
    type VARCHAR(5)
);

CREATE UNIQUE INDEX uind_link ON source(link);
CREATE UNIQUE INDEX uind_name ON source(name);
CREATE UNIQUE INDEX uind_linkName ON source(link, name);

--#######--

INSERT INTO source(name, link, thumbnail, type) values(
'BBC News',
'http://feeds.bbci.co.uk/news/rss.xml',
'https://pbs.twimg.com/profile_images/1001759983363641344/pJANPf_h_400x400.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'CNN',
'http://rss.cnn.com/rss/edition.rss',
'https://pbs.twimg.com/profile_images/508960761826131968/LnvhR8ED_400x400.png',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'NBA',
'http://feeds.feedburner.com/nba/PdXr',
'https://pbs.twimg.com/profile_images/921248739746033665/cjBVcCJG_400x400.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'ESPN FC',
'http://feeds.feedburner.com/espnfc/ziBg',
'https://pbs.twimg.com/profile_images/993576884498849792/zH7kViGI_400x400.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'Four Four Two',
'http://feeds.feedburner.com/fourfourtwo/qNct',
'https://the18.com/sites/default/files/FourFourTwo-Logo.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'Socrates Dergi',
'UCvgwLFmnppZoPVBQJwPaNsA',
'https://pbs.twimg.com/profile_images/936299754379673600/_qxGnuxw_400x400.jpg',
'YTB'
);

INSERT INTO source(name, link, thumbnail, type) values(
'BBC Technology',
'http://feeds.bbci.co.uk/news/technology/rss.xml',
'https://pbs.twimg.com/profile_images/1001759983363641344/pJANPf_h_400x400.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'CNN Technology',
'http://rss.cnn.com/rss/edition_technology.rss',
'https://pbs.twimg.com/profile_images/508960761826131968/LnvhR8ED_400x400.png',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'BBC Science',
'http://feeds.bbci.co.uk/news/science_and_environment/rss.xml',
'https://pbs.twimg.com/profile_images/1001759983363641344/pJANPf_h_400x400.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'CNN Space',
'http://rss.cnn.com/rss/edition_space.rss',
'https://pbs.twimg.com/profile_images/508960761826131968/LnvhR8ED_400x400.png',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'BBC Business',
'http://feeds.bbci.co.uk/news/business/rss.xml',
'https://pbs.twimg.com/profile_images/1001759983363641344/pJANPf_h_400x400.jpg',
'RSS'
);

INSERT INTO source(name, link, thumbnail, type) values(
'CNN Money',
'http://rss.cnn.com/rss/money_news_international.rss',
'https://pbs.twimg.com/profile_images/508960761826131968/LnvhR8ED_400x400.png',
'RSS'
);
